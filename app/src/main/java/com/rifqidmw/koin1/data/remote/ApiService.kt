package com.rifqidmw.koin1.data.remote

import com.google.gson.JsonObject
import com.rifqidmw.koin1.data.local.entity.AppVersion
import io.reactivex.Observable
import io.reactivex.Single
import okhttp3.ResponseBody
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.HeaderMap
import retrofit2.http.POST

interface ApiService {
    @POST("appVersion")
    fun appVersion(
        @HeaderMap headers: Map<String, String>,
        @Body data: JsonObject
    ) : Single<AppVersion>

    @GET("weixin706android1460.apk")
    fun getApk() : Observable<ResponseBody>
}