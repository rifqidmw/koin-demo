package com.rifqidmw.koin1.data.local

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy.REPLACE
import androidx.room.Query
import com.rifqidmw.koin1.data.local.entity.AppVersion

@Dao
interface MainDao {

    //GET MESSAGE PERSONAL
    @Query("SELECT * from app_version")
    fun getAppVersion(): List<AppVersion>

    @Insert(onConflict = REPLACE)
    fun addAppVersion(appVersion: AppVersion)

    @Query("UPDATE app_version SET description =:description")
    fun updateAppVersion(description: String)

    //DELETE CONTACT
    @Query("DELETE FROM app_version")
    fun deleteAllAppVersion()
}