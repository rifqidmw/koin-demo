package com.rifqidmw.koin1.data.local.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey

@Entity(tableName = "app_version", indices = [Index(value = ["version"], unique = true)])
data class AppVersion(@ColumnInfo(name = "version") var user_id: String,
                      @ColumnInfo(name = "description") var contact_name: String,
                      @PrimaryKey(autoGenerate = true)@ColumnInfo(name = "id") var id: Int = 0)