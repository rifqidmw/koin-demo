package com.rifqidmw.koin1.data.local

import android.content.Context
import android.content.SharedPreferences
import android.content.SharedPreferences.Editor

class Session(context: Context) {

    internal var public_MODE = 0

    init {
        preferences = context.getSharedPreferences(
            IS_NAV, public_MODE)
        editor = preferences.edit()
    }

    companion object {
        private val attempt = 1
        private val context: Context? = null
        private lateinit var preferences: SharedPreferences
        private lateinit var editor: Editor
        val IS_NAV = "DisableNavigation"
    }

    fun save(key: String, value: String) {
        editor.putString(key, value)
        editor.commit()
    }

    fun save(key: String, dataSet: HashSet<String>){
        editor.putStringSet(key, dataSet)
        editor.commit()
    }

    fun save(key: String, value: Int?) {
        save(key, value.toString())
    }

    fun save(key: String, value: Long?) {
        save(key, value.toString())
    }

    operator fun get(key: String): String? {
        return preferences.getString(key, null as String?)
    }

    operator fun contains(key: String): Boolean {
        return java.lang.Boolean.valueOf(preferences.contains(key))
    }

    fun removeKey(key: String) {
        editor.remove(key)
        editor.commit()
    }

    fun clear() {
        editor.clear()
        editor.commit()
    }

}