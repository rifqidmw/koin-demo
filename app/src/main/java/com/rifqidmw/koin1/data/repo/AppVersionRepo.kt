package com.rifqidmw.koin1.data.repo

import com.google.gson.JsonObject
import com.rifqidmw.koin1.data.local.entity.AppVersion
import com.rifqidmw.koin1.data.remote.ApiService
import io.reactivex.Single

class AppVersionRepo(private val service : ApiService) {

    fun getAppVersion(token: Map<String, String>, data: JsonObject): Single<AppVersion> {
        return service.appVersion(token,data)
    }
}