package com.rifqidmw.koin1.data.local

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.migration.Migration
import androidx.sqlite.db.SupportSQLiteDatabase
import com.rifqidmw.koin1.data.local.entity.AppVersion


@Database(
    entities = [AppVersion::class],
    version = 1,
    exportSchema = false)
abstract class MainDatabase : RoomDatabase() {

    abstract fun mainDao(): MainDao

    companion object {
        private var INSTANCE: MainDatabase? = null

        fun getInstance(context: Context): MainDatabase?{
            if (INSTANCE == null) {
                synchronized(MainDatabase::class) {
                    INSTANCE = Room.databaseBuilder(context.applicationContext,
                        MainDatabase::class.java, "yolo.db")
                        .allowMainThreadQueries()
                        .build()
                }
            }
            return INSTANCE
        }

        fun destroyInstance() {
            INSTANCE = null
        }

        val MIGRATION1_2 = object : Migration(1, 2) {
            override fun migrate(database: SupportSQLiteDatabase) {
                //example
                //database.execSQL("ALTER TABLE  chat_group_history  ADD COLUMN pinned TEXT")
               // database.execSQL("ALTER TABLE chat_personal_history ADD COLUMN pinnedTEXT")
            }
        }
        val MIGRATION2_3 = object : Migration(2, 3) {
            override fun migrate(database: SupportSQLiteDatabase) {
                //example
                database.execSQL("ALTER TABLE  chat_personal_history  ADD COLUMN about TEXT DEFAULT 'empty' NOT NULL ")
                database.execSQL("ALTER TABLE  contact  ADD COLUMN about TEXT DEFAULT 'empty' NOT NULL")
            }
        }

    }
}