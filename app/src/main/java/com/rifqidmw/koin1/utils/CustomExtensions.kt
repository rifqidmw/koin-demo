package com.rifqidmw.koin1.utils

import android.content.Context
import android.util.Log
import android.widget.Toast
import com.google.gson.Gson
import com.google.gson.JsonElement
import com.rifqidmw.koin1.BuildConfig

fun toJsonElement(any: Any): JsonElement = Gson().toJsonTree(any)


fun logI(tag: String, msg: String) {
    if (BuildConfig.DEBUG) Log.i(tag, msg)
}

fun logW(tag: String, msg: String) {
    if (BuildConfig.DEBUG) Log.w(tag, msg)
}

fun logE(tag: String, msg: String) {
    if (BuildConfig.DEBUG) Log.e(tag, msg)
}

fun logD(tag: String, msg: String) {
    if (BuildConfig.DEBUG) Log.d(tag, msg)
}

fun logV(tag: String,msg: String){
    if (BuildConfig.DEBUG) Log.v(tag,msg)
}

fun Context.toast(message :String){
    Toast.makeText(this,message, Toast.LENGTH_SHORT).show()
}