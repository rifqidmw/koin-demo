package mobile.id.hiapp.utils.mvvm

import androidx.annotation.CallSuper
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

abstract class RxViewModel<T> : ViewModel() {

    protected abstract val TAG: String
    private val disposable = CompositeDisposable()
    val state = MutableLiveData<T>()

    fun launch(job: () -> Disposable) {
        disposable.add(job())

    }

    @CallSuper
    override fun onCleared() {
        super.onCleared()
        disposable.clear()

    }


}