package com.rifqidmw.koin1.utils.downloadmanager

interface MyDownloadListener {
    fun onStartDownload()

    fun onProgress(progress: Int)

    fun onFinishDownload()

    fun onFail(errorInfo: String)
}