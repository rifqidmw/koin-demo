package mobile.id.hiapp.utils.mvvm

sealed class ViewModelState{
    data class Loading(var isLoading: Boolean) : ViewModelState()
    data class Failed(var error: String?="",var code:String?=null) : ViewModelState()
}
