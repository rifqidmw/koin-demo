package com.rifqidmw.koin1.utils.downloadmanager

import okhttp3.Interceptor
import okhttp3.Response
import okhttp3.ResponseBody
import java.io.IOException

class DownloadInterceptor(private val downloadListener: MyDownloadListener) : Interceptor {

    @Throws(IOException::class)
    override fun intercept(chain: Interceptor.Chain): Response {

        val response = chain.proceed(chain.request())

        return response.newBuilder()
            .body(DownloadResponseBody(response.body!!, downloadListener))
            .build()
    }
}