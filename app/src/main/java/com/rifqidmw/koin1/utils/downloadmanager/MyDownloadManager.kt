package com.rifqidmw.koin1.utils.downloadmanager

import android.annotation.SuppressLint
import com.rifqidmw.koin1.BuildConfig.BASE_URL_API
import com.rifqidmw.koin1.data.remote.ApiService
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.functions.Consumer
import io.reactivex.schedulers.Schedulers
import okhttp3.OkHttpClient
import okhttp3.ResponseBody
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import java.io.*
import java.util.concurrent.TimeUnit

class MyDownloadManager {
    private val TAG = this@MyDownloadManager.javaClass.simpleName

    private val DEFAULT_TIMEOUT = 15

    private lateinit var retrofit: Retrofit

    private lateinit var listener: MyDownloadListener

    private lateinit var baseUrl: String

    private val downloadUrl: String? = null

    constructor(baseUrl: String, listener: MyDownloadListener){

        this.baseUrl = baseUrl
        this.listener = listener

        val mInterceptor = DownloadInterceptor(listener)

        val httpClient = OkHttpClient.Builder()
            .addInterceptor(mInterceptor)
            .retryOnConnectionFailure(true)
            .connectTimeout(DEFAULT_TIMEOUT.toLong(), TimeUnit.SECONDS)
            .build()

        retrofit = Retrofit.Builder()
            .baseUrl(BASE_URL_API)
            .client(httpClient)
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .build()
    }

    @SuppressLint("CheckResult")
    fun download (filePath: String, fileName: String, subscriber: Consumer<ResponseBody>){

        listener.onStartDownload()

        retrofit.create(ApiService::class.java)
            .getApk()
            .subscribeOn(Schedulers.io())
            .unsubscribeOn(Schedulers.io())
            .observeOn(Schedulers.computation())
            .doOnNext{
                writeFile(it.byteStream(), it.contentLength(), filePath, fileName)
            }
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(subscriber)
    }

    private fun writeFile(inputString: InputStream, total: Long, filePath: String, fileName: String){
        var file = File(filePath, fileName)
        if (file.exists()){
            file.delete()
        }

        try {
            val fos = FileOutputStream(file)

            val b = ByteArray(1024 * 4)

            var sum = 0L
            var len = 0
            var off = 0

            while (inputString.read(b).apply { len = this } > 0){
                fos.write(b, off, len)
                sum += len.toLong()
                val progress = (sum * 1.0f / total * 100).toInt()

                listener.onProgress(progress)
            }

            inputString.close()
            fos.flush()
            fos.close()
            listener.onFinishDownload()
        } catch (e: FileNotFoundException){
            listener.onFail("FileNotFoundException")
        } catch (e: IOException){
            listener.onFail("IOException")
        }
    }
}