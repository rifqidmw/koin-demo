package com.rifqidmw.koin1

import android.content.Context
import com.google.gson.Gson
import com.rifqidmw.koin1.BuildConfig.BASE_URL_API
import com.rifqidmw.koin1.data.local.MainDatabase
import com.rifqidmw.koin1.data.local.Session
import com.rifqidmw.koin1.data.remote.ApiService
import com.rifqidmw.koin1.data.repo.AppVersionRepo
import com.rifqidmw.koin1.ui.main.MainViewModel
import io.reactivex.schedulers.Schedulers
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import org.koin.androidx.viewmodel.dsl.viewModel

val appModule = module {

    single { createOkHttpClient() }
    single { createRetrofit(get())  }
    single { createApiService(get()) }

    //repo
    factory { AppVersionRepo(get()) }

    //viewmodel
    viewModel { MainViewModel(get()) }
}


val dataModule = module {
    single { createSharedPref(androidContext()) }
    single{ createRoomDatabase(androidContext()) }
}

//app
fun createOkHttpClient(): OkHttpClient {

    val httpLoggingInterceptor = HttpLoggingInterceptor()
    httpLoggingInterceptor.level = HttpLoggingInterceptor.Level.BODY

    return OkHttpClient.Builder()
        .connectTimeout(60L, TimeUnit.SECONDS)
        .readTimeout(60L, TimeUnit.SECONDS)
        .writeTimeout(60L, TimeUnit.SECONDS)
        .addInterceptor(httpLoggingInterceptor)
        .build()
}

fun createRetrofit(okHttpClient: OkHttpClient): Retrofit {
    return Retrofit.Builder()
        .baseUrl(BASE_URL_API)
        .addConverterFactory(GsonConverterFactory.create(Gson()))
        .addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io()))
        .client(okHttpClient)
        .build()
}

fun createApiService(retrofit: Retrofit): ApiService = retrofit.create(ApiService::class.java)


fun createSharedPref(context: Context) : Session =
    Session(context)

fun createRoomDatabase(context: Context) : MainDatabase = MainDatabase.getInstance(context)!!