package com.rifqidmw.koin1.ui.main

import com.rifqidmw.koin1.data.local.entity.AppVersion

sealed class MainState {
    data class OnSuccessGetAppVersion(val appVersion: AppVersion) : MainState()
    data class OnLoading(val isLoading :Boolean) :MainState()
    data class OnErrorState(val t: Throwable) : MainState()
}
