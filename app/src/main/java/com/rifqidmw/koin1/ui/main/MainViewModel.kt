package com.rifqidmw.koin1.ui.main

import com.google.gson.JsonObject
import com.rifqidmw.koin1.data.local.entity.AppVersion
import com.rifqidmw.koin1.data.repo.AppVersionRepo
import com.rifqidmw.koin1.utils.logD
import com.rifqidmw.koin1.utils.logE
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import mobile.id.hiapp.utils.mvvm.RxViewModel

class MainViewModel (val appVersionRepo: AppVersionRepo) : RxViewModel<MainState>(), IMainViewModel{
    override val TAG: String = MainState::class.java.simpleName
    override fun successGetAppVersion(appVersion: AppVersion) {
        logD(TAG,"success get app version")
        state.value = MainState.OnSuccessGetAppVersion(appVersion)
        state.value = MainState.OnLoading(false)
    }

    override fun errorLoad(t: Throwable) {
        logE(TAG,"error load : ${t.message}")
        state.value = MainState.OnErrorState(t)
    }

    fun doGetAppVersion(token: Map<String, String>, data: JsonObject) {
        logD(TAG,"do load image $data")
        state.value = MainState.OnLoading(true)
        launch {
            appVersionRepo.getAppVersion(token,data)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::successGetAppVersion,this::errorLoad)
        }
    }
}