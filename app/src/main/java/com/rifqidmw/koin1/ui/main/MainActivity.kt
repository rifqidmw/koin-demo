package com.rifqidmw.koin1.ui.main

import android.Manifest
import android.os.Bundle
import android.os.Environment
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import com.google.gson.JsonObject
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.multi.DialogOnAnyDeniedMultiplePermissionsListener.Builder.withContext
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import com.rifqidmw.koin1.BuildConfig.BASE_URL_API
import com.rifqidmw.koin1.R
import com.rifqidmw.koin1.data.local.MainDatabase
import com.rifqidmw.koin1.data.local.Session
import com.rifqidmw.koin1.data.local.entity.AppVersion
import com.rifqidmw.koin1.utils.downloadmanager.MyDownloadListener
import com.rifqidmw.koin1.utils.downloadmanager.MyDownloadManager
import com.rifqidmw.koin1.utils.logE
import com.rifqidmw.koin1.utils.toast
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.functions.Consumer
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_main.*
import org.json.JSONException
import org.koin.android.ext.android.inject
import zlc.season.rxdownload4.download
import zlc.season.rxdownload4.file
import zlc.season.rxdownload4.utils.safeDispose
import java.util.*


class MainActivity : AppCompatActivity(), Observer<MainState> {

    private val TAG = MainActivity::class.java.simpleName

    private val mainDatabase: MainDatabase by inject()
    private val session: Session by inject()
    private val mViewModel: MainViewModel by inject()

    private val compositeDisposable = CompositeDisposable()
    private var disposable: Disposable? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

//        mViewModel.state.observe(this, this@MainActivity)

        session.save("APK", "Versi 1")
//        addAppVersion(AppVersion("123", "Rifqi"))
//        getAppVersion()

        btn_download.setOnClickListener {

            Dexter.withActivity(this@MainActivity)
                .withPermissions(
                    Manifest.permission.CAMERA,
                    Manifest.permission.INTERNET,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE,
                    Manifest.permission.READ_EXTERNAL_STORAGE
                ).withListener(object : MultiplePermissionsListener {
                    override fun onPermissionsChecked(report: MultiplePermissionsReport) { /* ... */
                        download()
                    }

                    override fun onPermissionRationaleShouldBeShown(
                        permissions: List<PermissionRequest>,
                        token: PermissionToken
                    ) { /* ... */
                    }
                }).check()

        }

        BASE_URL_API
    }

    private fun download(){
//        var url = "https://dldir1.qq.com/weixin/android/weixin706android1460.apk"
//        disposable = url.download()
//            .observeOn(AndroidSchedulers.mainThread())
//            .subscribeBy(
//                onNext = { progress ->
//                    //download progress
//                    btn_download.text = "${progress.downloadSizeStr()}/${progress.totalSizeStr()}"
//                    Log.d(TAG, "Download progress: $progress")
//                },
//                onComplete = {
//                    //download complete
//                    btn_download.text = "Open"
//                    Log.d(TAG, "file directory: ${url.file()}")
//                },
//                onError = {
//                    //download failed
//                    Log.e(TAG, "Download error: $it")
//                    btn_download.text = "Retry"
//                }
//            )

//        MyDownloadManager("", object : MyDownloadListener {
//            override fun onStartDownload() {
//                Log.e("download", "manager start")
//            }
//
//            override fun onProgress(progress: Int) {
//                Log.e("download", progress.toString())
//            }
//
//            override fun onFinishDownload() {
//                Log.e("download", "manager finish")
//            }
//
//            override fun onFail(errorInfo: String) {
//                Log.e("download", "manager error")
//            }
//        }).download("${Environment.getExternalStorageDirectory()}/DownloadTest/", "apk-tes.apk", Consumer {
//
//        })
    }

    private fun addAppVersion(appVersion: AppVersion) {
        compositeDisposable.add(Observable.fromCallable {
            mainDatabase.mainDao().addAppVersion(appVersion)
        }
            .subscribeOn(Schedulers.computation())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                Log.d("YOLO", "add starred message success")
            }, {
                Log.e("YOLO", "add starred message failed, $it")
            }))
    }

    private fun getAppVersion() {
        compositeDisposable.add(Observable.fromCallable {
            mainDatabase.mainDao().getAppVersion()
        }
            .subscribeOn(Schedulers.computation())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                Log.d("APPVERSION", it.joinToString())
            }, {
                Log.e("YOLO", "add starred message failed, $it")
            }))
    }

    private fun getAppVersionApi(){
        val headers = HashMap<String, String>()
        headers["Content-Type"] = "application/json"

        val jsonObject= JsonObject()
        try {
            jsonObject.addProperty("state_code", "123")
            jsonObject.addProperty("phone", "phoneNumber")

        }catch (e: JSONException){
            logE(TAG, "error send request get app version: ${e.toString()}")
        }
        mViewModel.doGetAppVersion(headers, jsonObject)

    }

    override fun onChanged(state: MainState?) {
        when(state){
            is MainState.OnSuccessGetAppVersion -> {
                val appVersion = state.appVersion
            }

            is MainState.OnErrorState -> {
                val errorState = state.t
                toast(errorState.toString())
            }

            is MainState.OnLoading -> {
                val isLoading = state.isLoading
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        disposable.safeDispose()
    }
}