package com.rifqidmw.koin1.ui.main

import com.rifqidmw.koin1.data.local.entity.AppVersion

interface IMainViewModel {
    fun successGetAppVersion(appVersion: AppVersion)
    fun errorLoad(t: Throwable)
}